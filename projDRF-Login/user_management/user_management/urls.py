from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from users.api.viewsets import GroupViewSet, get_update_delete_user
from users.api.viewsets import get_all_users, create_user, reset_password
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register(r'groups', GroupViewSet) # H) e I)

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    #path('api-auth/', include('rest_framework.urls')),
    path('api-token-auth/', obtain_auth_token), # B)
    path('users-change-pwd/<int:pk>/', reset_password), # F)
    path('users-create/', create_user), # A)
    path('users-list/', get_all_users), # C)
    path('users-detail/<int:pk>/', get_update_delete_user), # D), E) e G)
]

'''
# A) Endpoint for user's registration - To register a user you need
 to send an email, a password, a full name and the department he/she works
# B) Endpoint for user's authentication - To authenticate a user you need to
 send it's username and password.
# C) Endpoint to read the user profile list - Show only full name,
 and profile identifier
# D) Endpoint to read the user profile detail - Show all info
# E) Endpoint to update a user profile (email, full name and department)
# F) Endpoint to change the user's password - To change a password you
 need to send the old password and the new password
# G) Endpoint to delete a user profile
# H) Endpoint to create a department - To create a department you only need to send it's name
# I) Endpoint to delete a department
'''