#from user_management.departments.models import Depto
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework import status

class IntegrationTests(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='thalita', email='teste241118@teste.com',
                                                password='!QAZ2wsx3edc', first_name='Thalita', last_name='Nunes', groups=1)
        token = Token.objects.create(user=self.user)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

'''
    def test_create_deptos(self):
        dto = {
            "name": "first",
        }

        response = self.client.post(reverse('depts'), dto, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Depto.objects.count(), 1)

    def test_cant_create_depto_with_value_0(self):
        dto = {
            "name": "",
        }

        response = self.client.post(reverse('depts'), dto, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Depto.objects.count(), 0)
'''