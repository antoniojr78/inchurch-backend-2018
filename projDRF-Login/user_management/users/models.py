'''from django.db import models
from departments.models import Depto

class MyUser(models.Model):
    username = models.CharField(max_length=30,unique=True)
    fullname = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=30)
    superuser = models.BooleanField(default=False)
    id_depto = models.ForeignKey(Depto, on_delete=models.PROTECT, verbose_name='Departamento')

    def __str__(self):
        return self.username
'''