from django.contrib.auth.models import User, Group
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer, GroupSerializer, UserSerializerGetAllUsers
from .serializers import UserSerializerPutUser, UserResetPasswordSerializer
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status

class GroupViewSet(ModelViewSet):
    #API endpoint that allows groups to be viewed or edited.
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    authentication_classes = (TokenAuthentication,)
    # IsAuthenticated For all users who have token
    # IsAuthenticatedOrReadOnly, Read only for user without token
    # IsAdminUser Only for administrators who have token
    permission_classes = (IsAdminUser,)

@api_view(['PUT'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
#@permission_classes((IsSuperuserOrIsSelf,))
def reset_password(request, pk):
    # update password of a single user
    if request.method == 'PUT':
        try:
            iuser = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = UserResetPasswordSerializer(data=request.data)

        if serializer.is_valid():
            if not iuser.check_password(serializer.data.get('password')):
                return Response({'old_password': ['Wrong password.']},
                                status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            iuser.set_password(serializer.data.get('new_password'))
            iuser.save()
            return Response({'Detail': 'Password changed successfully.'}, status=status.HTTP_200_OK)

        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_all_users(request):
    # get all users
    if request.method == 'GET':
        user = User.objects.all()
        serializer = UserSerializerGetAllUsers(user, many=True)
        return Response(serializer.data)

@api_view(['POST'])
def create_user(request):
    # insert a new record for a user
    if request.method == 'POST':
        data = {
            'username': request.data.get('username'),
            'email': request.data.get('email'),
            'password': request.data.get('password'),
            'first_name': request.data.get('first_name'),
            'last_name': request.data.get('last_name'),
            #'groups': request.data.get('groups'),
        }
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            # encrypting the password
            u = User.objects.get(username=request.data.get('username'))
            u.set_password(request.data.get('password'))
            # Insert group list
            vgroups = request.data.get('groups')
            u.groups.set(vgroups) #table.field.set(list)
            u.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET','PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
#These function names will be displayed as title in the DRF
def get_update_delete_user(request, pk):
    try:
        iuser = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # get details of a single user
    if request.method == 'GET':
        serializer = UserSerializer(iuser)
        return Response(serializer.data)

    # update details of a single user
    if request.method == 'PUT':
        serializer = UserSerializerPutUser(iuser, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # delete a single puppy
    if request.method == 'DELETE':
        iuser.delete()
        return Response({'Detail': 'Successful Deletion.'}, status=status.HTTP_204_NO_CONTENT)