from rest_framework.serializers import ModelSerializer
from django.contrib.auth.models import User, Group
from rest_framework import serializers

# F)
class UserResetPasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(source='user.password', style={'input_type': 'password'},
        max_length=20, min_length=8)
    new_password = serializers.CharField(style={'input_type': 'password'},
        max_length=20, min_length=8)
    class Meta:
        model = User
        fields =("password", 'new_password')

# A) e D)
class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        # not returning in response of Json
        extra_kwargs = {'password': {'write_only': True}}
        fields = ('username','email','password','first_name','last_name','groups')

# H) e I)
class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)

# C)
class UserSerializerGetAllUsers(ModelSerializer):
    class Meta:
        model = User
        fields = ('id','first_name','last_name')

# E)
class UserSerializerPutUser(ModelSerializer):
    class Meta:
        model = User
        # I used another Serializer because of Django's required fields
        # For example 'username' and 'password' of the UserSerializer
        fields = ('email','first_name','last_name','groups')

class PasswordSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('password',)

'''
# A) Endpoint for user's registration - To register a user you need
 to send an email, a password, a full name and the department he/she works
# B) Endpoint for user's authentication - To authenticate a user you need to
 send it's username and password.
# C) Endpoint to read the user profile list - Show only full name,
 and profile identifier
# D) Endpoint to read the user profile detail - Show all info
# E) Endpoint to update a user profile (email, full name and department)
# F) Endpoint to change the user's password - To change a password you
 need to send the old password and the new password
# G) Endpoint to delete a user profile
# H) Endpoint to create a department - To create a department you only need to send it's name
# I) Endpoint to delete a department
'''